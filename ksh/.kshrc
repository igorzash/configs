export LC_ALL=en_US.UTF-8
export LANG=$LC_ALL
export PS1="\[\e[33m\]\w\[\e[m\] \\$ "
